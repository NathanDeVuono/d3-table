const path = require('path');
const webpack = require('webpack');
const config = require('./webpack.config.base');
const HtmlWebpackPlugin = require('html-webpack-plugin');

config.entry.app.push('./src/demo/index.jsx');
config.entry.app.unshift('react-hot-loader/patch');

config.output.path = path.resolve(__dirname, '../dev/');
config.output.chunkFilename = '[name].bundle.js';

config.devtool = 'source-map';

config.plugins.push(
  new webpack.DefinePlugin({
    DEV__: JSON.stringify('true')
  }),
  new HtmlWebpackPlugin()
);

const host = '0.0.0.0';
const port = 3000;

// add hot loader entry points to the start of app entry
config.entry.app.unshift(
  `webpack-dev-server/client?http://${host}:${port}`,
  'webpack/hot/only-dev-server'
);

// pass host and port to the devServer
config.devServer = {
  host,
  port
};

// add HotModuleReplacementPlugin
config.plugins.push(
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin()
);

// convert output path to absolute path
config.output.path = path.resolve(__dirname, config.output.path);

config.mode = 'development';

module.exports = config;
