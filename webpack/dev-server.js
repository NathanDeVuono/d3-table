const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config.dev');

const { host, port } = config.devServer;

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  stats: {
    colors: true
  },
  disableHostCheck: true
}).listen(port, host, err => {
  if (err) {
    return console.log(err);
  }

  console.log(`webpack-dev-server listening at ${host}:${port}`);
  return true;
});
