import React from 'react';
import { shallow } from 'enzyme';

import dummyRows from '../../demo/data.json';

import Column from './Column';

describe('Column', () => {
  it('should render', () => {
    const dom = shallow(<Column />);
    expect(dom).toMatchSnapshot();
  });

  it('should render with data', () => {
    const dom = shallow(<Column column={{ key: 'name' }} rows={dummyRows} />);
    expect(dom).toMatchSnapshot();
  });

  it('should render with a header prop', () => {
    const dom = shallow(
      <Column column={{ key: 'name', header: 'Name' }} rows={dummyRows} />
    );
    expect(dom).toMatchSnapshot();
  });

  it('should render custom headerRenderer', () => {
    const dom = shallow(
      <Column
        column={{
          key: 'name',
          header: 'Name',
          headerRenderer: data => <div>{data}</div>
        }}
        rows={dummyRows}
      />
    );
    expect(dom).toMatchSnapshot();
  });

  it('should pass down rowHeight to each cell', () => {
    const dom = shallow(
      <Column rowHeight={999} column={{ key: 'name' }} rows={dummyRows} />
    );
    expect(dom).toMatchSnapshot();
  });
});
